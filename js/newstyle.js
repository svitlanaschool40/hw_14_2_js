document.addEventListener("DOMContentLoaded", function() {
  let tabs = document.querySelectorAll(".tabs-title");
  let tabContents = document.querySelectorAll(".tabs-content li");

  function switchTab(tabIndex) {
    tabContents.forEach(function(content) {
      content.style.display = "none";
    });

    tabs.forEach(function(tab) {
      tab.classList.remove("active");
    });

    tabs[tabIndex].classList.add("active");
    tabContents[tabIndex].style.display = "block";
  }

  tabs.forEach(function(tab, index) {
    tab.addEventListener("click", function() {
      switchTab(index);
    });
  });

  switchTab(0);
});

document.addEventListener("DOMContentLoaded", function() {
  let themeToggle = document.getElementById("theme-toggle");
  let theme = localStorage.getItem("theme");

  if (theme) {
    document.body.classList.add(theme);
  }

  themeToggle.addEventListener("click", () => {
    if (document.body.classList.contains("light-theme")) {
      toggleTheme("light-theme", "dark-theme");
    } else {
      toggleTheme("dark-theme", "light-theme");
    }
  });

  function toggleTheme(oldTheme, newTheme) {
    document.body.classList.remove(oldTheme);
    document.body.classList.add(newTheme);
    localStorage.setItem("theme", newTheme);
  }
});